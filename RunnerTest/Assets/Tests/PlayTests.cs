﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class NewTestScript
    {


        [UnityTest]
        public IEnumerator Test_the_Player_away_Yeeter()
        {
            yield return null;


            //setup player
            GameObject Player = new GameObject("Player")
            {
                tag = "Player"
            };

            Player.AddComponent<Rigidbody>();
            Player.AddComponent<playerScript>();
            Player.GetComponent<Rigidbody>().useGravity = false;


            //setup droplet
            GameObject drop = new GameObject("Droplet");

            drop.AddComponent<Rigidbody>();
            drop.AddComponent<dropScript>();
            drop.SetActive(true);

            //execute testing line
            drop.GetComponent<dropScript>().GetYeeted(Player);


            Debug.Log(Player.GetComponent<Rigidbody>().velocity);


            //setup expected result
            Vector3 yeetvelocity = new Vector3(0, 5, -700);


            //test
            Assert.IsTrue(Player.GetComponent<Rigidbody>().velocity == yeetvelocity);            
        }



        [UnityTest]
        public IEnumerator Test_Player_Movement_And_Limiter()
        {
            yield return null;


            //setup player
            GameObject Player = new GameObject("Player");

            Player.AddComponent<Rigidbody>();
            Player.AddComponent<playerScript>();

            Vector3 startposition = new Vector3(0, 0, 0);
            Player.GetComponent<Rigidbody>().position = startposition;
            Player.GetComponent<Rigidbody>().useGravity = false;


            //execute testing line, multiple for limit testing
            Player.GetComponent<playerScript>().inputHandeling(Player.gameObject, true);
            Player.GetComponent<playerScript>().inputHandeling(Player.gameObject, true);
            Player.GetComponent<playerScript>().inputHandeling(Player.gameObject, true);


            //setup expected result
            Vector3 expectedPosition = new Vector3(-2.6f, 0, 0);


            //test
            Assert.IsTrue(Player.GetComponent<Rigidbody>().position == expectedPosition);
        }
    }
}
