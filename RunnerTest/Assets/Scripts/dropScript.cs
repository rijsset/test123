﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dropScript : MonoBehaviour
{
    Vector3 standardSpeed = new Vector3(0, 0, -60);
    Vector3 yeetSpeed = new Vector3(0, 5, -700);
    Rigidbody rbd;

    // Start is called before the first frame update
    void Start()
    {
        rbd = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        rbd.velocity = standardSpeed;

        if (transform.position.z < 0)
        {
            gameObject.SetActive(false);//by setting false, gets returned in pool in launcher
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        GetYeeted(collision.gameObject);
    }

    public void GetYeeted(GameObject game)
    {
        if (game.tag == "Player")
        {
            //Yeet away!!!!!!!!!!!!
            game.GetComponent<Rigidbody>().velocity = yeetSpeed;
        }
    }
}
