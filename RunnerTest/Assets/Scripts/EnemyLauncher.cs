﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class EnemyLauncher : MonoBehaviour
{
    public Dictionary<string, Queue<GameObject>> dropletDict;
    public List<Pool> pools;


    public Stopwatch timer;

    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject Droplet;
        public int size;
    }

    int rowNumber = -1;
    float[] nextRowNumber = {0, 2.6f, -2.6f, 0, 0, 2.6f, -2.6f, 0, -2.6f, 2.6f, -2.6f, 0, -2.6f, 2.6f, 2.6f, 0 , 0, 2.6f, 0, 2.6f, -2.6f, 0, -2.6f, 0, 0, 2.6f, -2.6f, 2.6f, -2.6f, 2.6f, -2.6f, 2.6f, -2.6f, 0, -2.6f, 0, 0, 2.6f, -2.6f, 2.6f, -2.6f, 0, 0, 2.6f, -2.6f, 2.6f, 0, 2.6f, 0, 2.6f, 0, 2.6f, -2.6f, 2.6f, -2.6f, 2.6f, -2.6f, 2.6f, -2.6f, 0 };
    // randomized with random.org

    float spawnPointZ = 50.0f;
    float spawnPointY = 1.5f;

    bool deploy = false;

    int currentSecCounter;

    void Start()
    {
        dropletDict = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in pools)
        {
            Queue<GameObject> dropPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.Droplet);
                obj.SetActive(false);
                dropPool.Enqueue(obj);
            }

            dropletDict.Add(pool.tag, dropPool);
        }


        timer = new Stopwatch();
        timer.Start();

    }

    // Update is called once per frame
    void Update()
    {
        if (deploy)
        {
            for (int i = 0; i < 2; i++) //spawn 2 drops
            {
                    rowNumber++;
                    if(rowNumber >= 60)
                    {
                        rowNumber = 0;
                    }
                    float xSpaceOnRow = nextRowNumber[rowNumber];//on predetermined seemingly random locations
                    Vector3 position = new Vector3(xSpaceOnRow, spawnPointY, spawnPointZ);

                    GameObject dropBoii = SpawnFromPool("Drops", position);
                
            }
            deploy = false;
        }
        else if(timer.ElapsedMilliseconds / 1000 % 2 == 0)
        {
            currentSecCounter++;
            if (currentSecCounter == 1)//stops it from keeping spawning
            {
                deploy = true;
            }
        }
        else
        {
            currentSecCounter = 0;
        }
    }


    public GameObject SpawnFromPool(string tag, Vector3 position)
    {
        if (!dropletDict.ContainsKey(tag))
        {
            print("You messed up spawning");
            return null;
        }

        GameObject toSpawnObject = dropletDict[tag].Dequeue();
        toSpawnObject.SetActive(true);
        toSpawnObject.transform.position = position;

        dropletDict[tag].Enqueue(toSpawnObject);

        return toSpawnObject;
    }
}
