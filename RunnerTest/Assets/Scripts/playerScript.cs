﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript : MonoBehaviour
{

    Rigidbody rbd;

    bool keyPressed;

    double maxLeft = -2.5;
    double maxRight = 2.5;

    int awayYeetedDistance = -700;
    Vector3 startPosition = new Vector3(0, 2.66f, 2);

    void Start()
    {
        rbd = GetComponent<Rigidbody>();
    }

    void Update()
    {

        if (rbd.position.z < awayYeetedDistance)
        {
            rbd.velocity = Vector3.zero;
            rbd.position = startPosition;
        }


        if (!Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow))
        {
            keyPressed = false;
        }

        if (!keyPressed)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {

                inputHandeling(this.gameObject, true);
                keyPressed = true;
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                inputHandeling(this.gameObject, false);
                keyPressed = true;
            }
        }
    }

    public void inputHandeling(GameObject thisObject, bool left)
    {
        float moveDistance = 2.6f;
        if (thisObject.GetComponent<Rigidbody>().position.x > maxLeft && left)
        {
            moveDistance *= -1;

            thisObject.GetComponent<Rigidbody>().position += Vector3.right * moveDistance;
        }
        else if(thisObject.GetComponent<Rigidbody>().position.x < maxRight && !left)
        {
            thisObject.GetComponent<Rigidbody>().position += Vector3.right * moveDistance;
        }

    }
}
